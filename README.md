## Topologia
Nossa topologia foi baseada principalmente em uma pesquisa realizada no site do dinf e pode conter algumas inconsistências.
Para simular a infraestrutura do DINF no mininet foram utilizados os seguintes switches e hosts:
* s_backb (switch do backbone, que liga a rede do dinf à RNP)
    * s_lab12 (switch laboratório 1 e 2)
        * h_lab12_x (0 <= x <=40)
    * s_lab3 (switch laboratório 3)
        * h_lab3_x (0 <= x <=20)
    * s_lab4 (switch laboratório 4)
        * h_lab4_x (0 <= x <=20)
    * s_c3sl (switch c3sl)
        * h_c3sl_x (0 <= x <=20)
    * s_ecomp (switch da ecomp)
        * h_ecomp_x (0 <= x <=10)
        * s_ecompw (roteador da ecomp, o mininet não diferencia conexões com e sem fio)
            * h_ecompw_x (0 <= x <= 5)
    * s_prof (switch salas dos professores)
        * h_prof_x (0 <= x <=40)
    * s_pesq (switch máquinas para laboratórios de pesquisas)
        * h_pesq_x (0 <= x <=30)
    * s_serv (switch das máquinas que servem sites do dinf)
        * h_serv_x (0 <= x <=12)

Num primeiro momento pretendiamos utilizar as especificações reais da rede, 
porém ao realizar os primeiros testes percebemos que seria muito dificil perceber os resultados, portanto, limitamos a banda da rede em 2Mbps


## Testes
O enunciado pedia que 4 testes fossem realizados
* Erro de encaminhamento (Forwarding error)
* Congestionamento (Congestion)
* Largura de banda disponível (Available bandwidth)
* Prioridade (Priority)

Para simular de maneira mais rápida e consistente, resolvemos criar alguns scripts (forwarding_error.py, congestion.py, priority.py), 
separados assim, apenas para que os resultados de um não influenciassem nos outros.


### Erro de encaminhamento
O teste de erro de encaminhamento foi realizado utilizando o comando **iptables**.
Com ele, é possível criar regras para aceitar ou recusar pacotes de determinados hosts.  
O comando executado foi:
```bash
iptables -A INPUT -i eth0 -j DROP -s 10.0.0.2
```
Ao executar esse comando no host 10.0.0.1 por exemplo, todos os pacotes vindos do host 10.0.0.2 são descartados. Assim, o teste pode ser realizado executando um ping do host 10.0.0.2 para o host 10.0.0.1.
```bash
mininet> 10.0.0.2 ping 10.0.0.1
```
Todos os pacotes serão perdidos.

Para executar nosso teste, basta rodar o script
```bash
sudo python forwarding_error.py
```

### Congestionamento
O teste de congestionamento foi realizado utilizando o comando **iperf**.
Com ele, é possível medir a largura de banda entre dois hosts.  
Inicialmente, o seguinte comando foi executado:
```bash
iperf -s h_lab12_0 -c h_lab12_1
```
Esse comando passa um tempo analisando a troca de pacotes entre hosts e sua saída é a vazão da conexão entre os dois (throughput), idealmente o resultado seria igual à capacidade do meio, 2Mbits/s, e isso é obtido quando o comando é executado.  
A parte importante desse teste vem em seguida, são  realizados diversos pings para os hosts que estão sendo analisados, a fim de congestionar o meio: 
```bash
mininet> h_lab12_2 ping h_lab12_0 -s 50000 -c 100 -i 0.001 &
mininet> h_lab12_3 ping h_lab12_1 -s 50000 -c 100 -i 0.001 &
```
Esses dois comandos fazem com que os hosts 2 e 3 do lab12 enviem aos hosts 0 e 1 100 pacotes de 50Kb com intervalos de 0.001 segundos entre cada pacote.  
Essa quantidade de pacotes e o curto intervalo entre cada envio rapidamente congestiona o meio, e nesse momento é executado o segundo iperf
```bash
iperf -s h_lab12_0 -c h_lab12_1
```
O resultado é uma diminuição considerável da largura de banda.


Em nosso teste, contido na função CongestionTest(), é realizada uma medição antes e durante o congestionamento, enviando uma especie de ataque DDOS dos hosts da ecomp e do lab12 à um dos servidores,
dessa forma, conseguiamos notar um congestionamento entre o

Todos os pacotes serão perdidos.

O teste pode ser executado com
```bash
sudo python congestion.py
```

### Largura de banda disponível
O teste da largura de banda é semelhante ao teste de congestionamento, porém o objetivo dele era diferente, identificar o link que causava um gargalo na conexão entre hosts.  
O teste foi realizado utilizando o Pathload, uma ferramenta que mede a largura de banda entre 2 links.  
Os seguintes comandos foram executados:
```bash
mininet> h_lab3_0 ./pathload_snd #prepara o lab3_0 pra enviar
mininet> h_lab12_0 ./pathload_rcv -s 10.0.0.41 #começa a receber do lab3_0
mininet> h_lab3_1 ./pathload_snd #prepara o lab3_1 pra enviar
mininet> h_lab4_1 ./pathload_rcv -s 10.0.0.42 #começa a receber do lab3_1
mininet> h_lab12_2 sudo ping -c 100 -i 1 -s 50000 h_c3sl_2 #envia pacotes para um host do c3s, para congestionar o link do lab12
mininet> h_lab12_3 sudo ping -c 100 -i 1 -s 50000 h_c3sl_3 #envia pacotes para um host do c3s, para congestionar o link do lab12
```
As primeiras duas linhas iniciam a medição de largura de banda entre h\_lab3\_0 e h\_lab12\_0, inicialmente mostram bons resultados.  
As linhas 3 e 4 iniciam a medição de largura de banda entre h\_lab3\_1 e h\_lab4\_1. Como todas as máquinas do lab3 estão conectadas num switch que está conectado no backbone, o link utilizado é o mesmo, logo a largura de banda tanto do h\_lab3\_0<-->h\_lab12\_0 quanto h\_lab3\_1<-->h\_lab4\_1 são a metade do link.  
As linhas 5 e 6 servem apenas para congestionar o link do lab12, com elas, a largura de banda de h\_lab3\_0<-->h\_lab12\_0 passa a ser muito menor do que a de h\_lab3\_1<-->h\_lab4\_1. Provando que o link do lab12 é o gargalo (bottleneck) na transmissão de pacotes do lab3 para o resto da rede.  
Esse teste infelizmente não consegue chegar ao final, já que é sempre interrompido pelo sistema, que mostra um erro de muitas tasks cpu-intensive.

### Prioridade

Para executar esse teste, utilizamos o comando **tc** (Traffic Control), utilizando o algoritimos de formatação de banda htb,
o código utilizado se encontra na função PriorityTest(). 

Nele são criadas 3 classes com prioridade 1,2,3,4, entre o switch backbone e o switch dos servidore, e alguns filtros para atribuição dessas classes de acordo com a porta utilizada (pesq, prof, lab3 respectivamente), além de ulm filtro "default" que aplica a classe 4;
Foram medidos os resultados antes e durante um congestionamento utilizando o iperf em 3 hosts.

Durante o congestionamento, notamos que no momento 20-25s ocorre a ação de controle de prioridade (onde a priorização ocorre da forma inversa ao que tinhamos préviamente definido)
Acreditamos que esse resultado equivocado se deva à erros na sintaxe do comando tc (que não foi compreendida por completo)

O teste pode ser executado com
```bash
sudo python priority.py
```

