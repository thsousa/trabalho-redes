#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call
from mininet.topo import Topo
from pprint import pprint
import re
import time

class Network():
    def createTopology(self):
        self.net = Mininet( topo=None,
                build=False,
                ipBase='10.0.0.0/8')


        info( '*** Add switches\n')
        # switch backb
        s_backb = self.net.addSwitch('s_backb', cls=OVSKernelSwitch, dpid='1')
        # switch lab 12
        s_lab12 = self.net.addSwitch('s_lab12', cls=OVSKernelSwitch, dpid='2')
        # switch lab 3
        s_lab3 = self.net.addSwitch('s_lab3', cls=OVSKernelSwitch, dpid='3')
        # switch lab 4
        s_lab4 = self.net.addSwitch('s_lab4', cls=OVSKernelSwitch, dpid='4')
        # switch ecomp wireless (o roteador nele eh considerado um switch)
        s_ecompw = self.net.addSwitch('s_ecompw', cls=OVSKernelSwitch, dpid='5')
        # switch ecomp
        s_ecomp = self.net.addSwitch('s_ecomp', cls=OVSKernelSwitch, dpid='6')
        # switch professores
        s_prof = self.net.addSwitch('s_prof', cls=OVSKernelSwitch, dpid='7')
        # switch c3sl
        s_c3sl = self.net.addSwitch('s_c3sl', cls=OVSKernelSwitch, dpid='8')
        # switch laboratorios de pesquisa
        s_pesq = self.net.addSwitch('s_pesq', cls=OVSKernelSwitch, dpid='9')
        # switch serv
        s_serv = self.net.addSwitch('s_serv', cls=OVSKernelSwitch, dpid='10')

        info( '*** Add hosts\n')
        h_lab12 = [self.net.addHost( 'h_lab12_%d' %i, cls=Host) for i in range(40)]
        h_lab3 = [self.net.addHost( 'h_lab3_%d' %i, cls=Host, defaultRoute=None) for i in range(20)]
        h_lab4 = [self.net.addHost( 'h_lab4_%d' %i, cls=Host) for i in range(20)]
        h_ecomp = [self.net.addHost( 'h_ecomp_%d' %i, cls=Host, defaultRoute=None) for i in range(10)]
        h_prof = [self.net.addHost( 'h_prof_%d' %i, cls=Host) for i in range(40)]
        h_c3sl = [self.net.addHost( 'h_c3sl_%d' %i, cls=Host) for i in range(20)]
        h_pesq = [self.net.addHost( 'h_pesq_%d' %i, cls=Host) for i in range(30)]
        # h_ecompw = [self.net.addHost( 'h_ecompw_%d' %i, cls=Host) for i in range(5)]
        h_serv = [self.net.addHost( 'h_serv_%d' %i, cls=Host, defaultRoute=None) for i in range(12)]


        info( '\n*** Add links\n')

        self.net.addLink(s_lab12, s_backb, cls=TCLink, bw=2, delay='5ms')
        self.net.addLink(s_lab3, s_backb, cls=TCLink, bw=2, delay='5ms')
        self.net.addLink(s_lab4, s_backb, cls=TCLink, bw=2, delay='5ms')
        self.net.addLink(s_serv, s_backb, cls=TCLink, bw=2, delay='5ms')
        self.net.addLink(s_ecomp, s_backb, cls=TCLink, bw=2, delay='5ms')
        self.net.addLink(s_prof, s_backb, cls=TCLink, bw=2, delay='5ms')
        self.net.addLink(s_c3sl, s_backb, cls=TCLink, bw=2, delay='5ms')
        self.net.addLink(s_pesq, s_backb, cls=TCLink, bw=2, delay='5ms')
        self.net.addLink(s_ecompw, s_ecomp, cls=TCLink, bw=2, delay='5ms')

        # considerando 1 =100 e 100 = 10000
        for h in h_lab12:
            self.net.addLink(h, s_lab12, cls=TCLink, bw=2)

        for h in h_lab3:
            self.net.addLink(h, s_lab3, cls=TCLink, bw=2)

        for h in h_lab4:
            self.net.addLink(h, s_lab4, cls=TCLink, bw=2)

        # for h in h_ecompw:
        #     self.net.addLink(h, s_ecompw, cls=TCLink, bw=1)
        for h in h_ecomp:
            self.net.addLink(h, s_ecomp, cls=TCLink, bw=2)

        for h in h_serv:
            self.net.addLink(h, s_serv, cls=TCLink, bw=2)

        for h in h_prof:
            self.net.addLink(h, s_prof, cls=TCLink, bw=2)
        
        for h in h_c3sl:
            self.net.addLink(h, s_c3sl, cls=TCLink, bw=2)
        
        for h in h_pesq:
            self.net.addLink(h, s_pesq, cls=TCLink, bw=2)

        info( '*** Adding controllers\n' )
        c_backb=self.net.addController(name='c_backb', protocol='tcp', port=6633)
        c_lab12=self.net.addController(name='c_lab12', protocol='tcp', port=6634)
        c_lab3=self.net.addController(name='c_lab3', protocol='tcp', port=6635)
        c_lab4=self.net.addController(name='c_lab4', protocol='tcp', port=6636)
        c_ecomp=self.net.addController(name='c_ecomp', protocol='tcp', port=6637)
        c_prof=self.net.addController(name='c_prof', protocol='tcp', port=6638)
        c_c3sl=self.net.addController(name='c_c3sl', protocol='tcp', port=6639)
        c_pesq=self.net.addController(name='c_pesq', protocol='tcp', port=6640)
        c_serv=self.net.addController(name='c_serv', protocol='tcp', port=6641)

        info( '\n*** Starting selfwork\n')
        self.net.build()

        info( '\n*** Starting controllers\n')
        for controller in self.net.controllers:
            controller.start()

        info( '\n*** Starting switches\n')
        self.net.get('s_backb').start([c_backb])
        self.net.get('s_lab12').start([c_lab12])
        self.net.get('s_lab3').start([c_lab3])
        self.net.get('s_lab4').start([c_lab4])
        self.net.get('s_ecompw').start([c_ecomp])
        self.net.get('s_ecomp').start([c_ecomp])
        self.net.get('s_prof').start([c_prof])
        self.net.get('s_c3sl').start([c_c3sl])
        self.net.get('s_pesq').start([c_pesq])
        self.net.get('s_serv').start([c_serv])
        info( '\n*** Started\n')

    def cli(self):
        info( '*** Post configure switches and hosts\n')
        CLI(self.net)
    
    def stop(self):
        info( '*** Stoping network\n')
        self.net.stop()


    def prepareHosts(self,name_list):
        self.hosts = {}
        for n in name_list:
            self.hosts[n]=[]

        for h in self.net.hosts:
            for n in name_list:
                if(re.match(r".*"+n+"_.*",h.name)):
                    self.hosts[n].append(h)
                    break  
    
    def prepareSwitches(self,name_list):
        self.switches = {}
        for n in name_list:
            self.switches[n]=[]

        for s in self.net.switches:
            for n in name_list:
                if(re.match(r".*"+n+"$",s.name)):
                    self.switches[n] = s
                    break  

    def forwardingErrorTest(self):

        info("\n**************************************")
        info("\n*** Running forwarding error test \n")
        
        s_backb = self.switches["backb"]
        s_prof = self.switches["prof"]
        s_lab3 = self.switches["lab3"]
        lab4_0 = self.hosts["lab4"][0]
        lab3_0 = self.hosts["lab3"][0]
        lab3_1 = self.hosts["lab3"][1]

        info("\n*** Creating rule on host lab4_0 to drop packets from host lab3_0 \n")
        lab4_0.cmd("iptables -A INPUT -i " +lab4_0.intfs[0].name + " -j DROP -s "+lab3_0.IP()+" &")
        
        info("\n*** Testing connection between lab4_0 to lab3_0 \n")
        self.net.ping((lab4_0,lab3_0),2)
        
        info("\n*** Testing connection between lab4_0 to lab3_1 \n")
        self.net.ping((lab4_0,lab3_1),2)

        info("\n*** Testing connection between lab3_0 to lab3_1 \n")
        self.net.ping((lab3_0,lab3_1),2)
        
        lab4_0.cmd("kill $!")

        info("\n**************************************")

    def congest(self,room,target, size=1000, qty=30, interval=1 ):
        info("\n*** Sending to "+ target.name + ": " + str(qty) + " packets of " + str(size) + " bytes from each host on "+ room +"\n")
        info("*** within an interval of " + str(interval) + "s\n")
        congestioning_hosts = self.hosts[room]
        for h in congestioning_hosts:
            h.cmd("ping "+target.IP()+" -s"+str(size)+" -c" + str(qty) + " -i " + str(interval)+" &")

    def flushFG(self,room):
        info("\n*** killing background processes \n")
        hosts = self.hosts[room]
        for h in hosts:
            h.cmd("kill $!")



    def congestionTest(self):
        info("\n**************************************")
        info("\n*** Running congestion test \n")

        lab3_0 = self.hosts["lab3"][0]
        serv_0 = self.hosts["serv"][0]
        serv_1 = self.hosts["serv"][1]
        serv_2 = self.hosts["serv"][2]
        serv_3 = self.hosts["serv"][3]
        

        info("\n*** Measuring performance before congestion\n")
        self.net.iperf((serv_0,lab3_0))

        self.congest("ecomp",serv_1,50000,6000,0.001)
        self.congest("lab12",serv_1,50000,6000,0.001)

        info("\n*** Measuring performance during congestion\n")
        self.net.iperf((serv_0,lab3_0))

        self.flushFG("ecomp")
        self.flushFG("lab12")
        info("\n**************************************")

    def descNode(self, node):
        info("*NAME: "+ node.name + " IP: "+ str(node.IP()) + "\n")

    def monitorOnInterval(self, node, interval, times=2):
        for i in range(1,times):
            time.sleep(interval)
            print node.monitor()

    def bandwidthTest(self):
        info("\n**************************************")
        info("\n**************************************")

    def priorityTest(self):
        
        info("\n**************************************")
        info("\n*** Running priority test \n")
        
        s_backb = self.switches["backb"]
        s_serv = self.switches["serv"]
        s_lab3 = self.switches["lab3"]
        s_prof = self.switches["prof"]
        s_pesq = self.switches["pesq"]

        backb_serv_intf = s_backb.connectionsTo(s_serv)[0][0].name

        c_backb_port = self.net.get('c_backb').port
        c_lab3_port = self.net.get('c_lab3').port
        c_prof_port = self.net.get('c_prof').port
        c_pesq_port = self.net.get('c_pesq').port

        info("\n*** creating priority queue and queue classes between on link backb-serv \n")
        s_backb.cmd("tc qdisc add dev " + backb_serv_intf + " root handle 1: htb")
        s_backb.cmd("tc qdisc add dev " + backb_serv_intf + " parent 1:0 classid 1:1 htb rate 1000 kbit")
        
        s_backb.cmd("tc qdisc add dev " + backb_serv_intf + " parent 1:1 classid 1:1 prio 1 htb rate 150 kbit ceil 200 kbit")
        s_backb.cmd("tc qdisc add dev " + backb_serv_intf + " parent 1:2 classid 1:2 prio 2 htb rate 60 kbit ceil 200 kbit ")
        s_backb.cmd("tc qdisc add dev " + backb_serv_intf + " parent 1:3 classid 1:3 prio 3 htb rate 20 kbit ceil 200 kbit")
        s_backb.cmd("tc qdisc add dev " + backb_serv_intf + " parent 1:4 classid 1:4 prio 4 htb rate 100 kbit ceil 500 kbit")
        
        info("\n** defining filter for pesq hosts to the priority 1 \n")
        s_backb.cmd("tc filter add dev " + backb_serv_intf + " parent 1: protocol ip u32 match ip port " + str(c_pesq_port) + " 0xffff flowid 1:1")
        info("\n** defining filter for prof hosts to the priority 2 \n")
        s_backb.cmd("tc filter add dev " + backb_serv_intf + " parent 1: protocol ip u32 match ip port " + str(c_prof_port) + " 0xffff flowid 1:2")
        info("\n** defining filter for lab3 hosts to the priority 3 \n")
        s_backb.cmd("tc filter add dev " + backb_serv_intf + " parent 1: protocol ip u32 match ip port " + str(c_lab3_port) + " 0xffff flowid 1:3")
        s_backb.cmd("tc filter add dev " + backb_serv_intf + " parent 1: protocol ip  flowid 1:4")

        serv_5 = self.hosts["serv"][5]  #congest
        serv_3 = self.hosts["serv"][3]  #acess with prof

        pesq_0 = self.hosts["pesq"][0]
        prof_0 = self.hosts["prof"][0]
        lab3_3 = self.hosts["lab3"][3]
        
        info("\n*** Hosts in action n\n")
        self.descNode(pesq_0)
        self.descNode(prof_0)
        self.descNode(lab3_3)

        info("\n*** Measuring performance before congestion\n\n")
        serv_3.cmd("iperf -s -i 5 &")
        pesq_0.cmd("iperf -c "+ serv_3.IP()+" -t 5 -n 1000000 &")
        prof_0.cmd("iperf -c "+ serv_3.IP()+" -t 5 -n 1000000 &")
        lab3_3.cmd("iperf -c "+ serv_3.IP()+" -t 5 -n 1000000 &")
        
        time.sleep(5)
        print serv_3.monitor()
        time.sleep(15)
        print serv_3.monitor()
       
        serv_3.cmd("kill $!")
        pesq_0.cmd("kill $!")
        prof_0.cmd("kill $!")
        lab3_3.cmd("kill $!")
        
        self.congest("c3sl",serv_5,50000,1500,0.01)

        info("\n*** Measuring performance during congestion\n\n")
        serv_3.cmd("iperf -s -i 5 &")
        pesq_0.cmd("iperf -c "+ serv_3.IP()+" -t 5 -n 1000000 &")
        prof_0.cmd("iperf -c "+ serv_3.IP()+" -t 5 -n 1000000 &")
        lab3_3.cmd("iperf -c "+ serv_3.IP()+" -t 5 -n 1000000 &")
        self.monitorOnInterval(serv_3,5,8)

        time.sleep(5)
        print serv_3.monitor()
        time.sleep(15)
        print serv_3.monitor()
        print pesq_0.monitor()
        print prof_0.monitor()
        print lab3_3.monitor()

        pesq_0.cmd("kill $!")
        prof_0.cmd("kill $!")
        lab3_3.cmd("kill $!")
        serv_3.cmd("kill $!")
      

        info("\n**************************************")

   
        
if __name__ == '__main__':
    setLogLevel( 'info' )
    net = Network()
    net.createTopology()

    # prepara array com os nomes 
    name_list = ["lab12","lab3","lab4","ecomp","ecompw","prof","serv","c3sl","pesq"]
    net.prepareHosts(name_list)

    name_list.append("backb")
    net.prepareSwitches(name_list)
    # net.forwardingErrorTest()
    # net.bandwidthTest()
    net.congestionTest()
    # net.priorityTest()
    # net.cli()
    net.stop()
